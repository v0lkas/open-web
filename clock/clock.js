var cl_format = [
//	  0		 1		 2					 3					 4		 5		 6		 7		 8
	['1',	'01',	'1<sub>am</sub>',	'01<sub>am</sub>',	'1',	'01',	'i',	'I',	'one'],
	['2',	'02',	'2<sub>am</sub>',	'02<sub>am</sub>',	'2',	'02',	'ii',	'II',	'two'],
	['3',	'03',	'3<sub>am</sub>',	'03<sub>am</sub>',	'3',	'03',	'iii',	'III',	'three'],
	['4',	'04',	'4<sub>am</sub>',	'04<sub>am</sub>',	'4',	'04',	'iv',	'IV',	'four'],
	['5',	'05',	'5<sub>am</sub>',	'05<sub>am</sub>',	'5',	'05',	'v',	'V',	'five'],
	['6',	'06',	'6<sub>am</sub>',	'06<sub>am</sub>',	'6',	'06',	'vi',	'VI',	'six'],
	['7',	'07',	'7<sub>am</sub>',	'07<sub>am</sub>',	'7',	'07',	'vii',	'VII',	'seven'],
	['8',	'08',	'8<sub>am</sub>',	'08<sub>am</sub>',	'8',	'08',	'viii',	'VIII',	'eight'],
	['9',	'09',	'9<sub>am</sub>',	'09<sub>am</sub>',	'9',	'09',	'ix',	'IX',	'nine'],
	['10',	'10',	'10<sub>am</sub>',	'10<sub>am</sub>',	'10',	'10',	'x',	'X',	'ten'],
	['11',	'11',	'11<sub>am</sub>',	'11<sub>am</sub>',	'11',	'11',	'xi',	'XI',	'eleven'],
	['12',	'12',	'12<sub>am</sub>',	'12<sub>am</sub>',	'12',	'12',	'xii',	'XII',	'twelve'],
	['13',	'13',	'1<sub>pm</sub>',	'01<sub>pm</sub>'],
	['14',	'14',	'2<sub>pm</sub>',	'02<sub>pm</sub>'],
	['15',	'15',	'3<sub>pm</sub>',	'03<sub>pm</sub>'],
	['16',	'16',	'4<sub>pm</sub>',	'04<sub>pm</sub>'],
	['17',	'17',	'5<sub>pm</sub>',	'05<sub>pm</sub>'],
	['18',	'18',	'6<sub>pm</sub>',	'06<sub>pm</sub>'],
	['19',	'19',	'7<sub>pm</sub>',	'07<sub>pm</sub>'],
	['20',	'20',	'8<sub>pm</sub>',	'08<sub>pm</sub>'],
	['21',	'21',	'9<sub>pm</sub>',	'09<sub>pm</sub>'],
	['22',	'22',	'10<sub>pm</sub>',	'10<sub>pm</sub>'],
	['23',	'23',	'11<sub>pm</sub>',	'11<sub>pm</sub>'],
	['24',	'24',	'12<sub>pm</sub>',	'12<sub>pm</sub>']
];

var cl_dots = ['|','*','&#730;','&#735;','&#8226;','&#730;','&#730;'];

var cl_cords = [
	[0.0000000000,100.0000000000],
	[1.7452406437, 99.9847695156],
	[3.4899496703, 99.9390827019],
	[5.2335956243, 99.8629534755],
	[6.9756473744, 99.7564050260],
	[8.7155742748, 99.6194698092],
	[10.4528463268,99.4521895368],
	[12.1869343405,99.2546151641],
	[13.9173100960,99.0268068742],
	[15.6434465040,98.7688340595],
	[17.3648177667,98.4807753012],
	[19.0808995377,98.1627183448],
	[20.7911690818,97.8147600734],
	[22.4951054344,97.4370064785],
	[24.1921895600,97.0295726276],
	[25.8819045103,96.5925826289],
	[27.5637355817,96.1261695938],
	[29.2371704723,95.6304755963],
	[30.9016994375,95.1056516295],
	[32.5568154457,94.5518575599],
	[34.2020143326,93.9692620786],
	[35.8367949545,93.3580426497],
	[37.4606593416,92.7183854567],
	[39.0731128489,92.0504853452],
	[40.6736643076,91.3545457643],
	[42.2618261741,90.6307787037],
	[43.8371146789,89.8794046299],
	[45.3990499740,89.1006524188],
	[46.9471562786,88.2947592859],
	[48.4809620246,87.4619707139],
	[50.0000000000,86.6025403784],
	[51.5038074910,85.7167300702],
	[52.9919264233,84.8048096156],
	[54.4639035015,83.8670567945],
	[55.9192903471,82.9037572555],
	[57.3576436351,81.9152044289],
	[58.7785252292,80.9016994375],
	[60.1815023152,79.8635510047],
	[61.5661475326,78.8010753607],
	[62.9320391050,77.7145961457],
	[64.2787609687,76.6044443119],
	[65.6059028991,75.4709580223],
	[66.9130606359,74.3144825477],
	[68.1998360062,73.1353701619],
	[69.4658370459,71.9339800339],
	[70.7106781187,70.7106781187]
];

// CLOCK DATA
var cl_size = 500;			// clock size (width & height) in px
var cl_radius = '260px';	// clock border radius
var cl_distance = 93;		// distance of numbers from center in %
var cl_frmt_opt = 1;		// CL_FORMAT option (type -1, if you do not want to use time stamps)
var cl_rotate = true;		// rotating numbers (true | false)

// DOTS DATA
var cl_dist_d = 97;		// distance of dots from center in %
var cl_size_dH = 10;	// size of hours dots in px
var cl_size_dM = 5;		// size of minutes dots in px
var cl_type_d = 0;		// width of hours line in px

// HOURS DATA
var cl_dist_h = 10;		// distance of hours line from center in %
var cl_size_h = 40;		// distance of hours line in %
var cl_width_h = 10;	// width of hours line in px

// MINUTES DATA
var cl_dist_m = 50;		// distance of minutes line from center in %
var cl_size_m = 35;		// distance of minutes line in %
var cl_width_m = 4;		// width of minutes line in px

// SECONDS DATA
var cl_dist_s = 97;		// distance of seconds line from center in %
var cl_size_s = 11;		// distance of seconds line in %
var cl_width_s = 2;		// width of seconds line in px

var cl_dist_cntr = cl_size / 100 * cl_distance / 2;
var prcnt_1 = 87;
var prcnt_2 = 50;

function launchClock() {
	document.getElementById("clock").style.width = cl_size+'px';
	document.getElementById("clock").style.height = cl_size+'px';
	document.getElementById("clock").style.borderRadius = cl_radius;
	
	var clock_left = (((document.body.clientWidth - cl_size) / 2).toFixed(0)) >= 10 ? ((document.body.clientWidth - cl_size) / 2).toFixed(0) : 10;
	var clock_top  = (((document.body.clientHeight - cl_size) / 2).toFixed(0)) >= 10 ? ((document.body.clientHeight - cl_size) / 2).toFixed(0) : 10;
	
	document.getElementById("clock").style.left = clock_left+'px';
	document.getElementById("clock").style.top  = clock_top+'px';
	
	// Printing clock
	if ((typeof cl_frmt_opt == "number" && cl_frmt_opt >= 0) || typeof cl_frmt_opt == "string") {
		if (cl_format[13][cl_frmt_opt]) {
			var cl_type = '24';
			
			var dt = new Date();
			var hr = dt.getHours();
			var mn = dt.getMinutes();
			var sc = dt.getSeconds();
			
			if(hr >= 12)
			{
				var cl_24 = true;
				var clock_change = ((24 - hr - 1) * 60 * 60 + (60 - mn - 1) * 60 + 60 - sc) * 1000;
				
				setTimeout(function() {
					changeTime(cl_frmt_opt,'am');
				},clock_change);
			}
			else
			{
				var cl_24 = false;
				var clock_change = ((12 - hr - 1) * 60 * 60 + (60 - mn - 1) * 60 + 60 - sc) * 1000;
				
				setTimeout(function() {
					changeTime(cl_frmt_opt,'pm');
				},clock_change);
			}
		}
		else var cl_type = '12';
	
		var clock_write = '<span id="clock_h"></span><span id="clock_m"></span><span id="clock_s"></span><ol id="clock_txt" style="margin:'+szConvert(45)+'px 0 0 '+szConvert(45)+'px">'
			for(var i=0;i<12;++i) {
				if(cl_type == '24' && cl_24 == true)	var hi = i + 12;
				else									var hi = i;
				
				clock_write += '<li>'+cl_format[hi][cl_frmt_opt]+'</li>';
			}
		clock_write += '</ol>';
		/*
		if(cl_type_d >= 0) {
			clock_write += '<ol id="clock_dots" style="margin:'+szConvert(45)+'px 0 0 '+szConvert(45)+'px">'
			for(var i=0;i<60;++i)
			{
				var cl_dot_dgr = i * 6;
				clock_write += '<li id="dot_'+cl_dot_dgr+'">'+cl_dots[cl_type_d]+'</li>';
			}
			clock_write += '</ol>';
		}
		*/
		
		document.getElementById('clock').innerHTML = clock_write;
		document.getElementById("clock_txt").style.fontSize = szConvert(5)+'px';

		// Dots counter
		/*
		if(document.getElementById("clock_dots").children[0]) {
			for(var i=0;i<60;++i)
			{
				var cl_dot_degree = i * 6;
				timeArrow('d',cl_dot_degree);
			}
		}
		*/
		
		// Hours counter
		if(document.getElementById("clock_txt").children[0]) {
			for(var i=0;i<12;++i) {
				if(cl_rotate == true) {
					var rotate = i * 30 + 30;
					document.getElementById("clock_txt").children[i].style.transform = 'rotate('+rotate+'deg)';
				}
				document.getElementById("clock_txt").children[i].style.width = szConvert(10)+'px';
				document.getElementById("clock_txt").children[i].style.height = szConvert(10)+'px';
				document.getElementById("clock_txt").children[i].style.lineHeight = szConvert(10)+'px';
				document.getElementById("clock_txt").children[i].style.borderRadius = szConvert(5)+'px';
				
				if((i > 0 && i < 4) || (i > 6 && i < 10)) {
					var vertical = prcnt_2;
					
					if(i == 2 || i == 8)	var horizontal = 100;
					else					var horizontal = prcnt_1;
				}
				else {
					var horizontal = prcnt_2;
					
					if(i == 5 || i == 11)	var vertical = 100;
					else					var vertical = prcnt_1;
				}
				
				if(i == 5 || i == 11)		var dirH = 0;
				else if(i <= 4)				var dirH = 1;
				else						var dirH = -1;
				
				if(i == 2 || i == 8)		var dirV = 0;
				else if(i <= 1 || i >= 9)	var dirV = -1;
				else						var dirV = 1;
				
				var mT = cl_dist_cntr / 100 * vertical * dirV;
				var mL = cl_dist_cntr / 100 * horizontal * dirH;
				
				document.getElementById("clock_txt").children[i].style.marginTop = mT+'px';
				document.getElementById("clock_txt").children[i].style.marginLeft = mL+'px';
			}
		}
	}
	else {
		var clock_write = '<span id="clock_h"></span><span id="clock_m"></span><span id="clock_s"></span>';
		document.getElementById('clock').innerHTML = clock_write;
	}
	
	timeRepeater();
	setInterval(function() {
		timeRepeater();
	},1000);
}

function szConvert(szInPrcnt) {
	return (cl_size / 100 * szInPrcnt).toFixed(0);
}

function timeRepeater() {
	timeArrow('h',false);
	timeArrow('m',false);
	timeArrow('s',false);
}

function timeArrow(id,angle) {
	// Time counting
	var dt = new Date();
	var hr = dt.getHours();
	var mn = dt.getMinutes();
	var sc = dt.getSeconds();
	
	if(id == 'h') {
		var ar_dist = cl_dist_h;
		var ar_size = cl_size_h;
		var ar_wdth = cl_width_h;
		
		if(hr > 12)		var hr = hr - 12;
		if(hr < 1)		var hr = 12;
		
		var cl_degree = ((360 / 12 * hr) + (mn / 2)).toFixed(0);
		
		if(cl_degree > 360)	cl_degree -= 360;
	}
	else {
		if(id == 'm') {
			var ar_dist = cl_dist_m;
			var ar_size = cl_size_m;
			var ar_wdth = cl_width_m;
			var ar_tp = mn;
		}
		else if(id == 's') {
			var ar_dist = cl_dist_s;
			var ar_size = cl_size_s;
			var ar_wdth = cl_width_s;
			var ar_tp = sc;
		}
		
		var cl_degree = (360 / 60 * ar_tp).toFixed(0);
	}
	
	if(angle)	var cl_degree = angle;
	
	var cl_arr_height = (cl_size / 200 * ar_size).toFixed(0);
	if(cl_arr_height / 2 != (cl_arr_height / 2).toFixed(0))	--cl_arr_height;
	
	if(cl_degree == 0 || cl_degree == 180) {
		var ar_left = ((cl_size / 2) - (ar_wdth / 2)).toFixed(0);
		if(cl_degree == 0)	var ar_top = (cl_size / 2) - cl_arr_height - (cl_size / 200 * ar_dist);
		else				var ar_top = (cl_size / 2) + (cl_size / 200 * ar_dist);
	}
	else if(cl_degree == 90 || cl_degree == 270) {
		var ar_top = ((cl_size / 2) - (cl_arr_height / 2)).toFixed(0);
		
		if(cl_degree == 90)	var ar_left = ((cl_size / 2) + (cl_arr_height / 2) + (cl_size / 200 * ar_dist)).toFixed(0);
		else				var ar_left = ((cl_size / 2) - (cl_arr_height / 2) - (cl_size / 200 * ar_dist)).toFixed(0);
	}
	else {
		// MEASUMENTS
		if(cl_degree <= 45 || (cl_degree > 135 && cl_degree <= 225) || cl_degree > 315) {
			if(cl_degree <= 45)			var tmp_degree = cl_degree;
			else if(cl_degree < 180)	var tmp_degree = 180 - cl_degree;
			else if(cl_degree <= 225)	var tmp_degree = cl_degree - 180;
			else						var tmp_degree = 360 - cl_degree;
			
			var tmp_wdth = (cl_arr_height / 100 * cl_cords[tmp_degree][0]).toFixed(0);
			var tmp_hght = (cl_arr_height / 100 * cl_cords[tmp_degree][1]).toFixed(0);
			
			var dst_wdth = (((cl_size / 2) / 100 * cl_cords[tmp_degree][0]) / 100 * ar_dist).toFixed(0);
			var dst_hght = (((cl_size / 2) / 100 * cl_cords[tmp_degree][1]) / 100 * ar_dist).toFixed(0);
		}
		else {
			if(cl_degree < 90)			var tmp_degree = 90 - cl_degree ;
			else if(cl_degree <= 135)	var tmp_degree = cl_degree - 90;
			else if(cl_degree < 270)	var tmp_degree = 270 - cl_degree;
			else						var tmp_degree = cl_degree - 270;
			
			var tmp_wdth = (cl_arr_height / 100 * cl_cords[tmp_degree][1]).toFixed(0);
			var tmp_hght = (cl_arr_height / 100 * cl_cords[tmp_degree][0]).toFixed(0);
			
			var dst_wdth = (((cl_size / 2) / 100 * cl_cords[tmp_degree][1]) / 100 * ar_dist).toFixed(0);
			var dst_hght = (((cl_size / 2) / 100 * cl_cords[tmp_degree][0]) / 100 * ar_dist).toFixed(0);
		}
		
		// MARGIN LEFT
		if(cl_degree < 180)	var ar_left = ((cl_size / 2) - -dst_wdth + (tmp_wdth / 2)).toFixed(0);
		else				var ar_left = ((cl_size / 2) - dst_wdth - (tmp_wdth / 2)).toFixed(0);
		
		// MARGIN TOP
		if(cl_degree > 90 && cl_degree < 270)	var ar_top = ((cl_size / 2) - -dst_hght - ((cl_arr_height - tmp_hght) / 2)).toFixed(0);
		else									var ar_top = ((cl_size / 2) - dst_hght - ((cl_arr_height - tmp_hght) / 2) - tmp_hght).toFixed(0);
	}
	
	if(!ar_left)	var ar_left = 0;
	if(!ar_top)		var ar_top = 0;
	
	document.getElementById('clock_'+id).style.width = ar_wdth+'px';
	document.getElementById('clock_'+id).style.height = cl_arr_height+'px';
	document.getElementById('clock_'+id).style.marginLeft = ar_left +'px';
	document.getElementById('clock_'+id).style.marginTop = ar_top +'px';
	document.getElementById('clock_'+id).style.transform = 'rotate('+cl_degree+'deg)';
}

function changeTime(option,period) {
	if(period == 'am')	var next_period = 'pm';
	else				var next_period = 'am';
	
	for(var i=0;i<12;++i) {
		if(document.getElementById("clock_txt").children[i]) {
			if(period == 'am')	var hi = i + 12;
			else				var hi = i;
			
			document.getElementById("clock_txt").children[i].innerHTML = cl_format[hi][option];
		}
	}
	
	setTimeout(function() {
		changeTime(option,next_period);
	},12*60*60*1000);
}