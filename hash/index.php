<!DOCTYPE html>
<html lang="en-GB">
<head>
	<title>Hash<?php
		if($_POST["word"] || $_GET["word"])	{
			$word = $_POST["word"] ? $_POST["word"] : $_GET["word"];
			echo ' ['.$word.']';
		}
	?> //VT</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="icon" href="http://www.vitalij.eu/images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" media="all" href="http://www.vitalij.eu/style.css">
	<style type="text/css">
		* {
			font-size:16px;
			font-weight:bold;
		}
		form {
			margin:0;
			padding:0;
		}
		input {
			width:75%;
			min-width:300px;
			max-width:600px;
			padding:10px;
			border:2px solid #c3c3c3;
			border-radius:10px;
			font-size:14px;
		}
		.content label {
			font-size:18px;
		}
		.content .hash {
			margin-bottom:10px;
		}
		.content .hash h2 {
			background:#f6f6f6;
			display:inline-block;
			margin:0 0 0 20px;
			padding:3px 8px;
			border-bottom:1px solid #c3c3c3;
			border-radius:3px;
		}
		.content .hash p {
			border-top:2px solid #c3c3c3;
			padding:15px 10px;
			display:block;
			margin:-11px 0 0 0;
		}
	</style>
</head>
<body>

<div class="content">
	<form action="./" method="post">
		<label for="word">Word(s) tho hash: </label><input type="text" name="word" id="word" value="<?php echo $word; ?>" onfocus="this.autocomplete='off';">
	</form>
</div>
<?php
	if($word) {
		$hashes = [
			'base64' => base64_encode($word),
			'crypt' => crypt($word),
			'crypt + base64_encode salt' => crypt($word,base64_encode($word)),
			'md5' => md5($word),
			'md5(md5)' => md5(md5($word)),
		];
		
		echo '<div class="content">';
			foreach($hashes as $name => $value) {
				echo '<div class="hash"><h2>'.$name.'</h2><p>'.$value.'</p></div>';
			}
		echo "\n</div>";
	}
?>

</body>
</html>